package main

import (
	"bytes"
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/sdk/logical"
)

// Simple exercise of the operationAuthUpdate method
func TestAuthUpdate(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}
	// Set up the mock
	mockedClient = newMockClient()

	// Set credentials
	authData := map[string]interface{}{
		"email":    "a@b.com",
		"password": "hunter2",
		"2fa":      "123456",
	}
	authReq := &logical.Request{
		Operation: logical.UpdateOperation,
		Storage:   config.StorageView,
		Path:      "auth",
		Data:      authData,
		EntityID:  "1234-8765",
	}
	_, err := b.HandleRequest(context.Background(), authReq)
	if err != nil {
		t.Fatalf("Error setting credentials: %v", err)
	}

	// Check that the client was stored
	reloadedClient, err := getClient(context.Background(), authReq)
	if err != nil {
		t.Fatalf("Error retrieving stored client: '%v'", err)
	}
	if reloadedClient.getAccessToken() != mockedClient.accessToken {
		t.Errorf("Expected access token '%s', got '%s'", mockedClient.accessToken, reloadedClient.getAccessToken())
	}
	if reloadedClient.getRefreshToken() != mockedClient.refreshToken {
		t.Errorf("Expected refresh token '%s', got '%s'", mockedClient.refreshToken, reloadedClient.getRefreshToken())
	}
	if reloadedClient.getExpiry() != mockedClient.expiry {
		t.Errorf("Expected expiry '%s', got '%s'", mockedClient.expiry, reloadedClient.getExpiry())
	}
	if reloadedClient.getLastRefresh() != mockedClient.lastRefresh {
		t.Errorf("Expected last refresh '%s', got '%s'", mockedClient.lastRefresh, reloadedClient.getLastRefresh())
	}
	if bytes.Compare(reloadedClient.getEncryptionKey(), mockedClient.encryptionKey) != 0 {
		t.Errorf("Expected encryption key '%s', got '%s'", mockedClient.encryptionKey, reloadedClient.getEncryptionKey())
	}
}

// Ensure that setting authentication credentials doesn't work
// without an Entity being associated with the call. This
// ensures that the credentials are only associated with a
// particular user, not globally available.
func TestAuthUpdateNoEntity(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}
	// Set up the mock
	mockedClient = newMockClient()

	// Set credentials
	authData := map[string]interface{}{
		"email":    "a@b.com",
		"password": "hunter2",
		"2fa":      "123456",
	}
	authReq := &logical.Request{
		Operation: logical.UpdateOperation,
		Storage:   config.StorageView,
		Path:      "auth",
		Data:      authData,
		// No EntityId
	}
	_, err := b.HandleRequest(context.Background(), authReq)
	if err == nil {
		t.Errorf("Credentials were allowed to be set without an entity")
	} else if err.Error() != "endpoint only available to callers tied to an identity" {
		t.Errorf("Unexpected error: '%v'", err)
	}
}

func TestAuthRead(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}
	// Set up the mock
	mockedClient = newMockClient()

	// Set credentials
	authData := map[string]interface{}{
		"email":    "a@b.com",
		"password": "hunter2",
		"2fa":      "123456",
	}
	authReq := &logical.Request{
		Operation: logical.UpdateOperation,
		Storage:   config.StorageView,
		Path:      "auth",
		Data:      authData,
		EntityID:  "1234-8765",
	}
	_, err := b.HandleRequest(context.Background(), authReq)
	if err != nil {
		t.Fatalf("Error setting credentials: '%v'", err)
	}

	// Reset the client for the read request
	b.getClient = getClient

	// Having written credentials to the endpoint, now read from the endpoint
	readReq := &logical.Request{
		Operation: logical.ReadOperation,
		Storage:   config.StorageView,
		Path:      "auth",
		EntityID:  "1234-8765",
	}
	resp, err := b.HandleRequest(context.Background(), readReq)
	if err != nil {
		t.Fatalf("Error reading auth/ path: '%v', err", err)
	}

	// Confirm that the reported data is correct
	if resp.Data["authenticated"] != true {
		t.Errorf("Expected to be authenticated")
	}
	expectedExpiry := mockedClient.lastRefresh.AddDate(0, 0, 30) // Yes, this is coupled to the implementation
	if resp.Data["expiry"] != fmt.Sprint(expectedExpiry) {
		t.Errorf("Expiry time incorrect. Expected '%s', got '%s'", fmt.Sprint(expectedExpiry), resp.Data["expiry"])
	}
	// Yes, this does run the risk of being out by one second depending on the exact time that
	// Now() was called in the function under test versus here, but it is very low chance, and
	// just rerunning the test in that case is not hard for this small project.
	expectedRemaining := formatRemaining(expectedExpiry.Sub(time.Now()))
	if resp.Data["remaining time"] != expectedRemaining {
		t.Errorf("Remaining time incorrect. Expected '%s', got '%s'", expectedRemaining, resp.Data["remaining time"])
	}
}

// Ahh, a straightforward function with no API calls, no
// side effects, no state, etc. Just an input and an output :-)
func TestFormatRemaining(t *testing.T) {
	pairs := []struct {
		time.Duration
		string
	}{
		{-12 * time.Second, "0 seconds"},
		{-12 * time.Hour, "0 seconds"},
		{5 * time.Second, "5 seconds"},
		{0, "0 seconds"},
		{1 * time.Minute, "1 minute"},
		{1*time.Minute + 14*time.Second, "1 minute 14 seconds"},
		{60 * time.Minute, "1 hour"},
		{20*time.Hour + 17*time.Minute + 63*time.Second, "20 hours 18 minutes 3 seconds"},
		{30*time.Hour + 23*time.Minute, "1 day 6 hours 23 minutes"},
		{62*24*time.Hour + 12*time.Second, "8 weeks 6 days 12 seconds"},
	}
	for _, pair := range pairs {
		remaining := pair.Duration
		expected := pair.string
		actual := formatRemaining(remaining)
		if expected != actual {
			t.Errorf("Bad time format: expected '%s', got '%s'", expected, actual)
		}
	}
}

func TestAuthDelete(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})
	entityID := "1234-8765"

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}
	// Set up the mock
	mockedClient = newMockClient()

	// Set credentials
	authData := map[string]interface{}{
		"email":    "a@b.com",
		"password": "hunter2",
		"2fa":      "123456",
	}
	authReq := &logical.Request{
		Operation: logical.UpdateOperation,
		Storage:   config.StorageView,
		Path:      "auth",
		Data:      authData,
		EntityID:  entityID,
	}
	_, err := b.HandleRequest(context.Background(), authReq)
	if err != nil {
		t.Fatalf("Error setting credentials: '%v'", err)
	}
	// Confirm that the existence helper sees the creds
	if !checkAreCredsStored(config.StorageView, entityID, t) {
		t.Fatalf("Credentials aren't stored - this could be a bug in the storage or the check")
	}

	// Reset the client for the delete request
	b.getClient = getClient

	// Having written credentials to the endpoint, now delete those credentials
	deleteReq := &logical.Request{
		Operation: logical.DeleteOperation,
		Storage:   config.StorageView,
		Path:      "auth",
		EntityID:  entityID,
	}
	_, err = b.HandleRequest(context.Background(), deleteReq)
	if err != nil {
		t.Fatalf("Error deleting auth/ path: '%v', err", err)
	}

	// Confirm that the credentials are gone
	credsExist := checkAreCredsStored(config.StorageView, entityID, t)
	if credsExist {
		t.Errorf("Credential data still present after delete")
	}
}

// Helper for abstracting out the check for existence of credentials
// in the storage.
func checkAreCredsStored(storage logical.Storage, entityID string, t *testing.T) bool {
	storageKey, err := getStorageKey(&logical.Request{
		EntityID: entityID,
	})
	if err != nil {
		t.Fatalf("Error getting storage key: '%v'", err)
	}
	data, err := storage.Get(context.Background(), storageKey)
	if err != nil {
		t.Fatalf("Error getting storage data: '%v'", err)
	}
	return data != nil
}
