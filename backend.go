package main

import (
	"context"
	"os"
	"strings"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
	"github.com/hashicorp/vault/sdk/plugin"
)

// This is set up as a global because this program is only
// ever invoked in response to a single Vault request, so
// there only needs to be one log interface. This avoids
// passing it around all over the place.
var logger hclog.Logger

func main() {
	apiClientMeta := &api.PluginAPIClientMeta{}
	flags := apiClientMeta.FlagSet()
	flags.Parse(os.Args[1:])

	tlsConfig := apiClientMeta.GetTLSConfig()
	tlsProviderFunc := api.VaultPluginTLSProvider(tlsConfig)

	err := plugin.Serve(&plugin.ServeOpts{
		BackendFactoryFunc: factory,
		TLSProviderFunc:    tlsProviderFunc,
	})
	if err != nil {
		logger := hclog.New(&hclog.LoggerOptions{})

		logger.Error("plugin shutting down", "error", err)
		os.Exit(1)
	}
}

func factory(ctx context.Context, conf *logical.BackendConfig) (logical.Backend, error) {
	logger = conf.Logger
	b := newBackend(conf, getClient)
	if err := b.Setup(ctx, conf); err != nil {
		return nil, err
	}
	return b, nil
}

type getClientFuncType func(context.Context, *logical.Request) (BwClientIfc, error)

type backend struct {
	*framework.Backend

	getClient getClientFuncType
	uuid      string
}

func newBackend(conf *logical.BackendConfig, getClientFunc getClientFuncType) *backend {
	b := backend{}
	b.Backend = &framework.Backend{
		Help: strings.TrimSpace(backendHelp),
		PathsSpecial: &logical.Paths{
			// I don't think this is used unless running an enterprise licence
			// and making use of a HKM or cloud KMS for a seal, but doesn't
			// hurt to specify it anyway.
			SealWrapStorage: []string{
				"auth/*",
			},
		},
		Paths: []*framework.Path{
			b.pathConfig(),
			b.pathAuth(),
			b.pathSecrets(),
		},
		BackendType: logical.TypeLogical,
	}
	b.getClient = getClientFunc
	b.uuid = conf.BackendUUID
	return &b
}

const backendHelp = `
The Bitwarden plugin allows users to read secrets from Bitwarden
through Vault. This is particularly useful for application access
to secrets using revokable/short-lived tokens, which Bitwarden
doesn't have support for.
`
