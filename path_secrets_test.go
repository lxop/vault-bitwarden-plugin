package main

import (
	"context"
	"fmt"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/sdk/logical"
)

func TestSecretRead(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}
	// Set up the mock
	secretData := map[string]string{
		"username": "frankie",
		"password": "goes to hollywood",
	}
	mockedClient = newMockClient()
	mockedClient.getSecretResponse = secretData

	// Read from a secret
	readReq := &logical.Request{
		Operation: logical.ReadOperation,
		Storage:   config.StorageView,
		Path:      "secrets/secret-name",
	}
	resp, err := b.HandleRequest(context.Background(), readReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: secret reading failed: resp:%#v\n err: %v", resp, err)
	}

	// Check the received data
	if equal, problems := compareSecretDicts(resp.Data, secretData); !equal {
		t.Errorf("Secrets not read correctly:\n%s", problems)
	}
}

func TestMissingSecretRead(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}
	// Set up the mock
	mockedClient = newMockClient()
	mockedClient.getSecretResponseError = &NotFoundError{}

	// Read from a secret
	readReq := &logical.Request{
		Operation: logical.ReadOperation,
		Storage:   config.StorageView,
		Path:      "secrets/missing-secret-name",
	}
	resp, err := b.HandleRequest(context.Background(), readReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: secret reading failed: resp:%#v\n err: %v", resp, err)
	}

	// Check that no secret is returned
	if resp != nil {
		t.Errorf("Unexpected secret data returned for missing secret: %v", resp.Data)
	}
}

func compareSecretDicts(actual map[string]interface{}, expected map[string]string) (bool, string) {
	mismatches := ""
	for ka, va := range actual {
		ve, ok := expected[ka]
		if !ok {
			mismatches += fmt.Sprintf("Key '%s' in actual but not expected\n", ka)
			continue
		}
		if ve != va {
			mismatches += fmt.Sprintf("Value for key '%s' mismatched: got '%s', expected '%s'\n", ka, va, ve)
		}
	}
	for ke := range expected {
		_, ok := actual[ke]
		if !ok {
			mismatches += fmt.Sprintf("Key '%s' in expected but not actual\n", ke)
		}
	}
	return mismatches == "", mismatches
}
