package main

import (
	"context"
	"crypto/rand"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/hashicorp/vault/sdk/logical"
)

type mockClient struct {
	baseURL       string
	encryptionKey []byte
	accessToken   string
	refreshToken  string
	expiry        time.Time
	lastRefresh   time.Time

	getRefreshExpiryResponse time.Time

	getSecretResponse      map[string]string
	getSecretResponseError error

	loginResponseError error
}

var mockedClient *mockClient

func newMockClient() *mockClient {
	return &mockClient{
		getSecretResponse:      make(map[string]string),
		getSecretResponseError: nil,
		loginResponseError:     nil,
	}
}

func getMockClient(context.Context, *logical.Request) (BwClientIfc, error) {
	if mockedClient == nil {
		mockedClient = newMockClient()
	}
	return mockedClient, nil
}

func (c *mockClient) getRefreshExpiry() time.Time {
	return c.getRefreshExpiryResponse
}

func (c *mockClient) login(email string, password string, code string, deviceUUID string) error {
	// If we are not returning an error, then set up all the bits that
	// ought to be set up by a successful login.
	if c.loginResponseError == nil {
		c.accessToken = uuid.New().String()
		c.refreshToken = uuid.New().String()
		// The Round call strips out the monotonic clock from the variable
		// so that useful comparisons can be performed in tests.
		c.lastRefresh = time.Now().Round(0)
		c.expiry = c.lastRefresh.Add(3600 * time.Second)
		c.encryptionKey = make([]byte, 64)
		n, err := rand.Reader.Read(c.encryptionKey)
		if err != nil {
			return fmt.Errorf("Failed to read random values into encryption key: %w", err)
		}
		if n != 64 {
			return fmt.Errorf("Failed to read 64 bytes into encryption key: got %d instead", n)
		}
	}
	return c.loginResponseError
}

func (c *mockClient) getSecret(name string) (map[string]string, error) {
	return c.getSecretResponse, c.getSecretResponseError
}

func (c *mockClient) refresh() error {
	return nil
}

func (c *mockClient) isAuthenticated() bool {
	return c.accessToken != ""
}

func (c *mockClient) getEncryptionKey() []byte {
	return c.encryptionKey
}

func (c *mockClient) getAccessToken() string {
	return c.accessToken
}

func (c *mockClient) getRefreshToken() string {
	return c.refreshToken
}

func (c *mockClient) getExpiry() time.Time {
	return c.expiry
}

func (c *mockClient) getLastRefresh() time.Time {
	return c.lastRefresh
}
