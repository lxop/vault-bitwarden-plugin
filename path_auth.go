package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hako/durafmt"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

func (b *backend) pathAuth() *framework.Path {
	return &framework.Path{
		Pattern: "auth",
		Fields: map[string]*framework.FieldSchema{
			"email": {
				Type:        framework.TypeString,
				Description: "Bitwarden account email address",
				Required:    true,
			},
			"password": {
				Type:        framework.TypeString,
				Description: "Bitwarden password",
				Required:    true,
			},
			"2fa": {
				Type:        framework.TypeString,
				Description: "2FA code",
				Required:    true,
			},
		},
		Operations: map[logical.Operation]framework.OperationHandler{
			logical.UpdateOperation: &framework.PathOperation{
				Callback: b.operationAuthUpdate,
			},
			logical.ReadOperation: &framework.PathOperation{
				Callback: b.operationAuthRead,
			},
			logical.DeleteOperation: &framework.PathOperation{
				Callback: b.operationAuthDelete,
			},
		},
		HelpSynopsis:    pathAuthRootHelpSyn,
		HelpDescription: pathAuthRootHelpDesc,
	}
}

func (b *backend) operationAuthUpdate(ctx context.Context, req *logical.Request, data *framework.FieldData) (*logical.Response, error) {
	// Extract request details
	email := data.Get("email").(string)
	password := data.Get("password").(string)
	code := data.Get("2fa").(string)
	// Drop trailing newlines so that this works properly in the case
	// where the user passed their password via stdin
	password = strings.TrimRight(password, "\n\r")
	if email == "" || password == "" || code == "" {
		return nil, logical.CodedError(http.StatusBadRequest, "'email', 'password', and '2fa' must be supplied")
	}

	// Get the bitwarden client
	client, err := b.getClient(ctx, req)
	if err != nil {
		return nil, err
	}

	// Send login request to bitwarden
	err = client.login(email, password, code, b.uuid)
	if err != nil {
		return nil, err
	}

	// Store the retrieved auth data
	if err = storeClient(ctx, req, client); err != nil {
		return nil, err
	}

	return nil, nil
}

// The default format for a Duration is xxhxxmxx.xxs, which
// is a bit crap when the duration is multiple days. So this
// function produces a string with a 'days' component too.
func formatRemaining(remaining time.Duration) string {
	if remaining < 0 {
		remaining = 0
	}
	remaining = remaining.Round(time.Second)
	return durafmt.Parse(remaining).String()
}

func (b *backend) operationAuthRead(ctx context.Context, req *logical.Request, _ *framework.FieldData) (*logical.Response, error) {
	client, err := b.getClient(ctx, req)
	if err != nil {
		return nil, err
	}
	authenticated := client.isAuthenticated()
	responseData := map[string]interface{}{
		"authenticated": authenticated,
	}
	if authenticated {
		expiry := client.getRefreshExpiry()
		responseData["expiry"] = fmt.Sprint(expiry)
		remaining := expiry.Sub(time.Now())
		responseData["remaining time"] = formatRemaining(remaining)
		responseData["note"] = "The expiry will be extended by reading a secret"
	}

	return &logical.Response{
		Data: responseData,
	}, nil
}

func (b *backend) operationAuthDelete(ctx context.Context, req *logical.Request, _ *framework.FieldData) (*logical.Response, error) {
	key, err := getStorageKey(req)
	if err != nil {
		return nil, err
	}
	if err := req.Storage.Delete(ctx, key); err != nil {
		return nil, fmt.Errorf("Failure while deleting auth from storage: %w", err)
	}
	return nil, nil
}

const pathAuthRootHelpSyn = `
Log-in to Bitwarden to enable future requests for secrets
`

const pathAuthRootHelpDesc = `
To interact with Bitwarden, Vault needs credentials for the
API and decryption keys for secret details.
This endpoint is used to log-in to Bitwarden and obtain these 
details on a per-user basis - the same path is used by all 
users, but the credentials are stored (and used) according to 
the calling user.
`
