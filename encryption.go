package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"golang.org/x/crypto/pbkdf2"
)

// This file contains logic for encrypting and decrypting things
// for sending to and receiving from Bitwarden.

func hashPassword(masterKey []byte, password string) string {
	// 256 bits, but pbkdf2 takes a count in bytes
	return base64.StdEncoding.EncodeToString(pbkdf2.Key(masterKey, []byte(password), 1, 256/8, sha256.New))
}

func makeMasterKey(password string, email string, iterations int) []byte {
	return pbkdf2.Key([]byte(password), []byte(strings.ToLower(email)), iterations, 256/8, sha256.New)
}

// regex encType.iv64|data64|mackey64
var re = regexp.MustCompile(`^(?P<enctype>\d)\.(?P<iv>[^|]+)\|(?P<payload>[^|]+)\|(?P<mac>[^|]+)$`)

func decryptCipherstring(cipherstring string, key []byte) ([]byte, error) {
	m := re.FindStringSubmatch(cipherstring)
	if m == nil {
		// Don't include the cipherstring in the error message, as it is
		// a secret, even if it is encrypted.
		return nil, fmt.Errorf("Bad format for cipherstring")
	}
	encType, errType := strconv.Atoi(m[1])
	iv, errIV := base64.StdEncoding.DecodeString(m[2])
	payload, errData := base64.StdEncoding.DecodeString(m[3])
	receivedMac, errMac := base64.StdEncoding.DecodeString(m[4])
	if errType != nil || errIV != nil || errData != nil || errMac != nil {
		return nil, fmt.Errorf("Failed to decode cipherstring: %w", first(errType, errIV, errData, errMac))
	}
	if encType != 2 {
		// 2 = AesCbc256_HmacSha256_B64
		return nil, fmt.Errorf("Unsupported cipherstring type %d", encType)
	}

	// Split the encryption key into its two parts
	encKey := key[:32]
	macKey := key[32:]

	// Verify MAC
	macData := append(iv, payload...)
	mac := hmac.New(sha256.New, macKey)
	mac.Write(macData)
	computedMac := mac.Sum(nil)
	if !hmac.Equal(receivedMac, computedMac) {
		return nil, errors.New("MAC verification failed")
	}

	// Decrypt the payload
	return decryptAES(payload, iv, encKey)
}

func decryptAES(payload []byte, iv []byte, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("Failed to create AES cipher: %w", err)
	}
	cbc := cipher.NewCBCDecrypter(block, iv)
	plaintext := make([]byte, len(payload))
	cbc.CryptBlocks(plaintext, payload)
	// Remove padding
	padding := plaintext[len(plaintext)-1]
	plaintext = plaintext[:len(plaintext)-int(padding)]
	return plaintext, nil
}

func stretchMasterKey(masterKey []byte) []byte {
	return append(hkdfExpand(masterKey, "enc"), hkdfExpand(masterKey, "mac")...)
}

func hkdfExpand(prk []byte, info string) []byte {
	msg := append([]byte(info), 1)
	mac := hmac.New(sha256.New, prk)
	mac.Write(msg)
	return mac.Sum(nil)
}

func first(errs ...error) error {
	for _, err := range errs {
		if err != nil {
			return err
		}
	}
	return nil
}

func decryptOrgKey(cipherString string, privateKeyDER []byte) ([]byte, error) {
	parts := strings.Split(cipherString, ".")
	if len(parts) != 2 {
		return nil, errors.New("Bad cipherstring format")
	}
	if parts[0] != "4" {
		// 4 = Rsa2048_OaepSha1_B64
		return nil, fmt.Errorf("Unsupported orgkey cipherstring type: %s", parts[0])
	}
	rawPayload, err := base64.StdEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("Failed to decode orgkey payload: %w", err)
	}
	privateKeyIfc, err := x509.ParsePKCS8PrivateKey(privateKeyDER)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse private key: %w", err)
	}
	privateKey, ok := privateKeyIfc.(*rsa.PrivateKey)
	if !ok {
		return nil, fmt.Errorf("Private key is not an RSA private key apparently")
	}
	orgKey, err := rsa.DecryptOAEP(sha1.New(), rand.Reader, privateKey, rawPayload, []byte{})
	if err != nil {
		return nil, fmt.Errorf("Failed to decrypt org key: %w", err)
	}
	return orgKey, nil
}
