package main

import (
	"bytes"
	"encoding/base64"
	"testing"
)

func TestMakeMasterKey(t *testing.T) {
	masterKey := makeMasterKey("hunter2", "azure@diamond.com", 100000)
	key64 := base64.StdEncoding.EncodeToString(masterKey)
	expected := "LYryf3KEuW+MQwrK9fHKdGQXyRz7Ev+0EZm0qa3OOcs="

	if key64 != expected {
		t.Error("Master key is not correct")
	}
}

func TestStretchMasterKey(t *testing.T) {
	masterKey := makeMasterKey("hunter2", "azure@diamond.com", 100000)
	stretched := stretchMasterKey(masterKey)
	stretched64 := base64.StdEncoding.EncodeToString(stretched)
	expected := "E4L7EBToulykB8QqhMXRODN5anoRDIUFc+/etfIo3OnJI9qNfueE66SVRwSe9OV+7gmDTfq+Dej9tkAclDy21g=="

	if stretched64 != expected {
		t.Error("Stretched master key is not correct")
	}
}

func TestHashPassword(t *testing.T) {
	pw := "p4$$w0rD"
	masterKey := makeMasterKey(pw, "foo@bar.com", 100000)
	hashed := hashPassword(masterKey, pw)
	expected := "Fgt+jzoRII8gUiKdmNyLMyrSXWb5XzI4rPvBYv96yEA="

	if hashed != expected {
		t.Error("Hashed password is not correct")
	}
}

func TestDecryptCipherString(t *testing.T) {
	key, err := base64.StdEncoding.DecodeString("MbRvr/eUg30eeDHRUpa+l57KxU1WZXgePE5nDpOSXU+2XAnQOU+3KVvfKtQzhNp3gEooUG+9soA54g2xENml0Q==")
	cipherstring := "2.iyBioP3HkmJsRwN5MSJHHg==|psl58u64wLaMhx/PXTEHVqFkNyxnGXzNsij2F4glklA=|DeHRXU1G5axOsuw89O1i7HbM2CrQplmyZB2wornFdC4="
	expectedPlaintext := "Judy makes tasty corn frittatas"
	if err != nil {
		t.Fatalf("Decoding the encryption key failed: %v", err)
	}

	plaintext, err := decryptCipherstring(cipherstring, key)
	if err != nil {
		t.Fatalf("Failed to decrypt cipherstring: err: %v", err)
	}

	if string(plaintext) != expectedPlaintext {
		t.Errorf("Decryption was incorrect - expected '%s', got '%s'", expectedPlaintext, plaintext)
	}
}

func TestDecryptBadMac(t *testing.T) {
	// Check that a corrupted cipherstring fails appropriately
	key, err := base64.StdEncoding.DecodeString("MbRvr/eUg30eeDHRUpa+l57KxU1WZXgePE5nDpOSXU+2XAnQOU+3KVvfKtQzhNp3gEooUG+9soA54g2xENml0Q==")
	cipherstring := "2.iyBioP3HkmJsRwN5MSJHHg==|psl58u64wLaMhx/PXTEHVqFkNyxnGXzNsij2F4glklA=|EeHRXU1G5axOsuw89O1i7HbM2CrQplmyZB2wornFdC4="
	if err != nil {
		t.Fatalf("Decoding the encryption key failed: %v", err)
	}

	_, err = decryptCipherstring(cipherstring, key)
	expectedError := "MAC verification failed"
	if err == nil {
		t.Errorf("Decryption of corrupted cipherstring did not fail")
	} else if err.Error() != expectedError {
		t.Fatalf("Unexpected error: '%s' (expected '%s')", err.Error(), expectedError)
	}
}

func TestDecryptOrgKey(t *testing.T) {
	// Arrange
	cipherstring := "4.LrKolDK7R2j9oHtBxNtAPUF9PQGljhVay+Yfiut/gLnOrzvZHa8sETduxkhN+84JWIFJE148RLHHG8a3IzBfvm76PAtC+dise4mRhJpvEcQAxA9BuQQI/G5KF6llgovtrbKNxZJ/Jlu1Q7KOB9HDK7n44BIIg9fCVSw1X4TvWudN5qjH81Hc2Ns685+aI/T/WvfCcp8OBxqePlKIyCVI5bhMd0ScKzYZ28vMNjgAO4Er3NKFGOykRSreYxTC/GMcFA0rvZVjbXzUNqey3dO0S5VgYHeNFb8owFt7IseumRMtIHfFnihUYL753PJqrOSt6lUdQVWITvsy1heO7NLX+A=="
	expectedOrgKey, err := base64.StdEncoding.DecodeString("WumARsKeQxsagdQvbZ4vII6GQSbHYDuurg3UGjwD5XW/abWuQkVpzrA/SswDAGl5roRvQk+i+rVdUhx8ldsg0w==")
	if err != nil {
		t.Fatalf("Decoding the expected org key failed: %v", err)
	}
	privateKey, err := base64.StdEncoding.DecodeString("MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDjqtGIERi5ImhhIJvvdqEsGD3jI++WuVqIUisId0k+oqHCufBW/712BDC5DZYY7felW9sLskT60dw1f9sa42IhWs1LSDo80/gTPD4yrmSxC9hG4xgz2QsL+ZH7wW0NUkcDpXq2Ow7jLLc4nWsfRrDn+/waKvODA9YYHas4C5IYbUexX8hycCr8ri+Wpa9UOqylVPAMhpXICX8U+a00zUizUYdeEDFsQoAvFl+b4CnW7PI4o8eP0ET2hyQk5qU2aF9VZQH5367AMOijbkCmgRs+NaoInZbUVDSrBOxp40glbtnvNEleVqQ2tKdJ8GQVaGERqhTDIptdPRT6rKSAtbNRAgMBAAECggEBAKuT/gusztQf0Uh52etTnU3tCx8W4ybVlBwuyM29kI7K5Fwr46y9Wh0KNWoRijBJj5yoqGGdPY1GAc60B/2wpjD7DMMSsQlMuqZMfZeWhL5Fbf6mgYP5q/CfatywPq6OdoSR/vQ9eA/PkFnzkVrl3MdIygsymYkAqCZOnc5EMhuDPDFeZ4g2mfPD/oqBak1u2EpGjUURTdxXNtzVnIsaeg/J0UsJGwIONOy03kQPkKO8/oa+o4ghS9Eekf1p7q+7N6fgqj+KRT9CjsfUWEQvp84ImL/RRs4GKhOJeo0eFsOcss5nJB7ukG7uURTXwHSszcNT333JgkhEzXicWCWBUY0CgYEA+YkHjfgmWCgoafoQLpfaUvq4JAgXaXkx6hGD4xbeAV+e6RBxn9FM83nwj9oLDyaPFLAYHj2mrRv4Xeah3+SdP6pAxXcjellp8wR2LGth1Nr0BvaSbUhIW4p3sKXpgGC6quTBhyhe6CLllDDulQf8gEjVlh3kaYo1Nhgdfa005yMCgYEA6ZDBdkhaeI2980W+/Vl18yYQqw35M0FVoSxCXvO2edB/beoMpdrfChaGNSJmdNJunam7dsgMaZTvluqJGsqDpXXOM3BrpPdIrBn4IrMSEYF/dRK/lXalMhuFbAnCD/NASr5+tU3vpvwSShRafZjp5KvRGHOSsI2TjAfPsXnH3PsCgYEA1h4R4G09Icobm4waVJCyox9F4I0c4cE/5lRvMe4OQ2l0ZxJYHUWwXP+WPyle8fopLhxIJbLLEaF9hb1/yylVESsa9xXa2IJe2ktKx6TAxq+2Uz9CuAkdJYd0WpyS9ziJFFjEAyDOByziiD1Qi4rVzJIyy+QMec9BB5C7xXpZebcCgYAK5CUhTt5pILLq1SXCypklbXD5iP1F1pYXnei6ZMLtiztl9LQCR+J+pe48LA57BBUEZRXIxCtQARHrB6S9CxR4crXa0DM0zhMlc1COzaraBh/bZbRpU88ANyhRkWLrO3yT/64I5kl2j+qKaciOcsL1Vs25Ripg8TkgHn0g2XzoYQKBgQC6aedzCcqyeKQMUK0VwuUkKUOio9uFfBadMJiLQnIhFWz6stVLpQ1Ylhh4FWAicVVhKh2qeFqHapWd3GcbHffpvnBBiGxjbuHFClP+oRAavsmn9OQTJN6dQJ29mgbVTu7f5CDElhi5885VfSwWLFeG4zLdWNyzr8YAIPQims++6w==")
	if err != nil {
		t.Fatalf("Decoding the private key failed: %v", err)
	}

	// Act
	orgKey, err := decryptOrgKey(cipherstring, privateKey)
	if err != nil {
		t.Fatalf("Failed to decrypt cipherstring: err: %v", err)
	}

	// Assert
	if bytes.Compare(orgKey, expectedOrgKey) != 0 {
		t.Errorf("Decrypted org key incorrectly")
	}
}
