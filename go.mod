module bitwarden_plugin

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/hako/durafmt v0.0.0-20191009132224-3f39dc1ed9f4
	github.com/hashicorp/go-hclog v0.13.0
	github.com/hashicorp/vault/api v1.0.4
	github.com/hashicorp/vault/sdk v0.1.13
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
)
