package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

func (b *backend) pathSecrets() *framework.Path {
	return &framework.Path{
		Pattern: "secrets/" + GenericNameWithSpacesRegex("name"),
		Fields: map[string]*framework.FieldSchema{
			"name": {
				Type:        framework.TypeString,
				Description: "The name of the secret to retrieve from Bitwarden",
				Required:    true,
			},
		},
		Operations: map[logical.Operation]framework.OperationHandler{
			logical.ReadOperation: &framework.PathOperation{
				Callback: b.operationSecretRead,
			},
		},
		HelpSynopsis:    pathConfigRootHelpSyn,
		HelpDescription: pathConfigRootHelpDesc,
	}
}

// GenericNameWithSpacesRegex returns a generic regex that allows alphanumeric
// characters along with -, ., @, and spaces.
func GenericNameWithSpacesRegex(name string) string {
	return fmt.Sprintf("(?P<%s>\\w(([\\w-.@ ]+)?\\w)?)", name)
}

func (b *backend) operationSecretRead(ctx context.Context, req *logical.Request, data *framework.FieldData) (*logical.Response, error) {
	// Extract the name of the secret
	nameIfc, ok := data.GetOk("name")
	if !ok {
		return nil, logical.CodedError(http.StatusBadRequest, "No secret name given")
	}
	name := nameIfc.(string)

	client, err := b.getClient(ctx, req)
	if err != nil {
		return nil, err
	}

	// Make a call out to BW
	secret, err := client.getSecret(name)
	if err != nil {
		if _, ok := err.(*NotFoundError); ok {
			// Not an error to not find anything, there's just
			// nothing there, so return nothing
			return nil, nil
		}
		return nil, err
	}

	// Convert between "types" (come on, Go...)
	responseData := make(map[string]interface{})
	for key, val := range secret {
		responseData[key] = val
	}

	// Format the response
	return &logical.Response{
		Data: responseData,
	}, nil
}
