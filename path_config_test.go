package main

import (
	"context"
	"reflect"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/sdk/logical"
)

func TestBackend_PathConfigWriteRead(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}

	// Write to /config
	configData := map[string]interface{}{
		"url": "test-value",
	}
	writeReq := &logical.Request{
		Operation: logical.UpdateOperation,
		Storage:   config.StorageView,
		Path:      "config",
		Data:      configData,
	}
	resp, err := b.HandleRequest(context.Background(), writeReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: config writing failed: resp:%#v\n err: %v", resp, err)
	}

	// Read back the value from /config
	readReq := &logical.Request{
		Operation: logical.ReadOperation,
		Storage:   config.StorageView,
		Path:      "config",
	}
	resp, err = b.HandleRequest(context.Background(), readReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: config reading failed: resp:%#v\n err: %v", resp, err)
	}

	// Confirm that the data matches
	if !reflect.DeepEqual(resp.Data, configData) {
		t.Errorf("bad: expected to read config as %#v, got %#v instead", configData, resp.Data)
	}
}

func TestBackend_PathConfigDelete(t *testing.T) {
	// Prepare the scaffolding
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}
	logger = hclog.New(&hclog.LoggerOptions{
		Level: hclog.Error + 1, // Suppress all logs
	})

	b := newBackend(config, getMockClient)
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}

	// Write to /config
	configData := map[string]interface{}{
		"url": "test-value12",
	}
	writeReq := &logical.Request{
		Operation: logical.UpdateOperation,
		Storage:   config.StorageView,
		Path:      "config",
		Data:      configData,
	}
	resp, err := b.HandleRequest(context.Background(), writeReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: config writing failed: resp:%#v\n err: %v", resp, err)
	}

	// Delete the value from /config
	deleteReq := &logical.Request{
		Operation: logical.DeleteOperation,
		Storage:   config.StorageView,
		Path:      "config",
	}
	resp, err = b.HandleRequest(context.Background(), deleteReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: config delete failed: resp:%#v\n err: %v", resp, err)
	}

	// Try to read back the value from /config
	readReq := &logical.Request{
		Operation: logical.ReadOperation,
		Storage:   config.StorageView,
		Path:      "config",
	}
	resp, err = b.HandleRequest(context.Background(), readReq)
	if err != nil || (resp != nil && resp.IsError()) {
		t.Fatalf("bad: config reading failed: resp:%#v\n err: %v", resp, err)
	}

	// Confirm that the data no longer exists
	if resp != nil {
		t.Errorf("bad: expected to read nil config, got %#v instead", resp.Data)
	}
}
