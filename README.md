This is a relatively use-case-specific backend for Hashicorp Vault to allow access
to secrets held in Bitwarden.

The situation that lead to this backend was:
- Secrets were stored in Bitwarden
- No intention to migrate the secrets to a new Vault installation, as they were 
  largely used by people rather than direct application access
- A desire to use short-lived tokens for access to secrets from the deployment
- A requirement not to have duplicate storage of secrets

Bitwarden has a better interface for simple human user access, but
Vault has support for short-lived tokens. Since we couldn't duplicate
secrets across both tools, I made use of the extensibility of Vault
and wrote a backend that allowed access to secrets stored in Bitwarden
via Vault.

This plugin only supports reading secrets from Bitwarden; it has no
support for writing them. In my opinion the main contribution of this
project is to demonstrate a Bitwarden client in Go. Since Bitwarden
uses client-side encryption, all interaction requires a complete pull
of all secrets and decryption of them all to find the desired one.

We made active use of this plugin for several months in 2020, so it is tested and
works; however, it is no longer actively developed and I do not intend
to extend the functionality to make it more generally applicable.
Please feel free to use the code as a reference or a base for other
purposes, but I am unlikely to accept merge requests.
