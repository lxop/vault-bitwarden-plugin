package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/hashicorp/vault/sdk/logical"
)

// This file contains functions for interacting with
// Bitwarden - login, pull, etc

// BwClientIfc defines the functions offered by the client,
// and exists primarily to allow for the use of a mock by tests.
type BwClientIfc interface {
	getRefreshExpiry() time.Time
	login(email string, password string, code string, deviceUUID string) error
	getSecret(name string) (map[string]string, error)
	refresh() error
	isAuthenticated() bool

	getEncryptionKey() []byte
	getAccessToken() string
	getRefreshToken() string
	getExpiry() time.Time
	getLastRefresh() time.Time
}

// BwClient is the client to enable access to stuff on the Bitwarden
// server.
type BwClient struct {
	baseURL       string
	encryptionKey []byte
	accessToken   string
	refreshToken  string
	expiry        time.Time
	lastRefresh   time.Time
}

func (c *BwClient) isAuthenticated() bool {
	return c.accessToken != ""
}

func (c *BwClient) getEncryptionKey() []byte {
	return c.encryptionKey
}

func (c *BwClient) getAccessToken() string {
	return c.accessToken
}

func (c *BwClient) getRefreshToken() string {
	return c.refreshToken
}

func (c *BwClient) getExpiry() time.Time {
	return c.expiry
}

func (c *BwClient) getLastRefresh() time.Time {
	return c.lastRefresh
}

// This client has a 30 day sliding refresh window and 1 hour access token expiry
const clientID = "desktop"

func (c *BwClient) getRefreshExpiry() time.Time {
	// The refresh token expires 30 days after the last login or refresh
	return c.lastRefresh.AddDate(0, 0, 30)
}

func (c *BwClient) login(email string, password string, code string, deviceUUID string) error {
	if c.baseURL == "" {
		return NotConfiguredError
	}

	iterations, err := prelogin(c.baseURL, email)
	if err != nil {
		return err
	}
	masterKey := makeMasterKey(password, email, iterations)
	hashedPassword := hashPassword(masterKey, password)

	reqBody := url.Values{
		"grant_type":        {"password"},
		"username":          {email},
		"password":          {hashedPassword},
		"scope":             {"api offline_access"}, // offline_access means we get a refresh_token
		"client_id":         {clientID},
		"deviceType":        {"8"}, // 8 = Linux Desktop
		"deviceIdentifier":  {deviceUUID},
		"deviceName":        {"vault"},
		"twoFactorToken":    {code},
		"twoFactorProvider": {"0"}, // 0 = authenticator
	}

	now := time.Now()
	resp, err := http.PostForm(c.baseURL+"/identity/connect/token", reqBody)
	if err != nil {
		return fmt.Errorf("Error posting to login endpoint: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return handleErrorResponse(resp)
	}

	type responseType struct {
		AccessToken  string `json:"access_token"`
		ExpiresIn    int    `json:"expires_in"`
		RefreshToken string `json:"refresh_token"`
		Key          string `json:"Key"`
	}
	decodedResponse := responseType{}
	err = json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		// I'm not including the underlying error in this message
		// just in case one of the tokens ends up in the message.
		return fmt.Errorf("Failed to decode login response")
	}

	stretchedMasterKey := stretchMasterKey(masterKey)
	encryptionKey, err := decryptCipherstring(decodedResponse.Key, stretchedMasterKey)
	if err != nil {
		return err
	}

	// Fill all the fields here to avoid making a partially complete object
	// in the case where an error happens somewhere in this method
	c.encryptionKey = encryptionKey
	c.accessToken = decodedResponse.AccessToken
	c.refreshToken = decodedResponse.RefreshToken
	c.expiry = now.Add(time.Second * time.Duration(decodedResponse.ExpiresIn))
	c.lastRefresh = now

	return nil
}

func handleErrorResponse(resp *http.Response) error {
	if resp.StatusCode == http.StatusUnauthorized {
		return logical.CodedError(http.StatusUnauthorized, "Not authenticated")
	}
	type responseType struct {
		Error            string `json:"error"`
		ErrorDescription string `json:"error_description"`
		ErrorModel       struct {
			Message string `json:"Message"`
		} `json:"ErrorModel"`
	}
	decodedResponse := responseType{}
	err := json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		if err == io.EOF {
			// This just means the response body was empty. So all we
			// have to work with is the status code
			return fmt.Errorf("Bitwarden error: %d %s", resp.StatusCode, http.StatusText(resp.StatusCode))
		}
		return fmt.Errorf("Failed to decode %d error response: %w", resp.StatusCode, err)
	}

	if decodedResponse.ErrorModel.Message != "" {
		// In this case it's already a nicely formatted error message
		return errors.New(decodedResponse.ErrorModel.Message)
	}
	// Otherwise, build something that might be useful
	return fmt.Errorf("Error (%d): %s: %s", resp.StatusCode, decodedResponse.Error, decodedResponse.ErrorDescription)
}

// prelogin finds the number of iterations of the KDF to use for
// this user
func prelogin(baseURL string, email string) (int, error) {
	reqBody, err := json.Marshal(map[string]string{
		"email": email,
	})
	if err != nil {
		return 0, fmt.Errorf("Failed to marshal prelogin request body: %w", err)
	}

	resp, err := http.Post(baseURL+"/api/accounts/prelogin", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		return 0, fmt.Errorf("Error posting to prelogin: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return 0, handleErrorResponse(resp)
	}
	type responseType struct {
		Kdf        int `json:"Kdf"`
		Iterations int `json:"KdfIterations"`
	}
	result := responseType{}
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return 0, fmt.Errorf("Failed to decode prelogin response: %w", err)
	}

	// 0 = PBKDF2_SHA256, and is the only type that is available
	if result.Kdf != 0 {
		return 0, fmt.Errorf("Prelogin reported unexpected KDF: %d", result.Kdf)
	}
	return result.Iterations, nil
}

func (c *BwClient) getSecret(name string) (map[string]string, error) {
	if c.accessToken == "" {
		return nil, logical.CodedError(http.StatusUnauthorized, "Not authenticated yet")
	}

	// Ensure the token will be valid for long enough to complete the request
	// 10 seconds seems like ample time, but it's just a heuristic.
	requiredValidThrough := time.Now().Add(10 * time.Second)
	if c.expiry.Before(requiredValidThrough) {
		if err := c.refresh(); err != nil {
			return nil, err
		}
		if c.expiry.Before(requiredValidThrough) {
			return nil, logical.CodedError(http.StatusUnauthorized, "Access token still expired after refreshing. Please re-authenticate")
		}
	}

	// Pull down the whole vault (sync)
	req, err := http.NewRequest("GET", c.baseURL+"/api/sync", nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to create GET request: %w", err)
	}
	req.Header.Add("Authorization", "Bearer "+c.accessToken)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("GET request failed: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, handleErrorResponse(resp)
	}

	result := syncResponseType{}
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		// I'm not including the underlying error here just in case
		// some of the pulled vault is in the message.
		return nil, fmt.Errorf("Failed to decode sync response")
	}

	return c.findSecret(&result, name)
}

type syncResponseType struct {
	Profile struct {
		PrivateKey    cipherString `json:"PrivateKey"`
		Organizations []struct {
			ID  string `json:"Id"`
			Key string `json:"Key"`
		} `json:"Organizations"`
	} `json:"Profile"`
	Ciphers []bwCipher `json:"Ciphers"`
}

func (data *syncResponseType) getOrgKeys(encryptionKey []byte) (map[string][]byte, error) {
	// Organisation encryption keys are encrypted with the user's public
	// key, so need to be decrypted with the user's private key.
	// That in turn is encrypted with the user's normal encryption key, so
	// decrypt that first.
	privateKey, err := data.Profile.PrivateKey.decrypt(encryptionKey)
	if err != nil {
		return nil, err
	}
	orgKeys := make(map[string][]byte)
	for _, org := range data.Profile.Organizations {
		key, err := decryptOrgKey(org.Key, privateKey)
		if err != nil {
			return nil, err
		}
		orgKeys[org.ID] = key
	}
	return orgKeys, nil
}

func (c *BwClient) refresh() error {
	reqBody := url.Values{
		"grant_type":    {"refresh_token"},
		"client_id":     {clientID},
		"refresh_token": {c.refreshToken},
	}
	now := time.Now()
	resp, err := http.PostForm(c.baseURL+"/identity/connect/token", reqBody)
	if err != nil {
		return fmt.Errorf("refresh failed: %w", err)
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return handleErrorResponse(resp)
	}
	type responseType struct {
		AccessToken  string `json:"access_token"`
		ExpiresIn    int    `json:"expires_in"`
		RefreshToken string `json:"refresh_token"`
	}
	decodedResponse := responseType{}
	err = json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		// Leaving out the underlying error to prevent accidental
		// exposure of any tokens
		return fmt.Errorf("Failed to decode refresh response")
	}

	c.accessToken = decodedResponse.AccessToken
	c.refreshToken = decodedResponse.RefreshToken
	c.expiry = now.Add(time.Second * time.Duration(decodedResponse.ExpiresIn))
	c.lastRefresh = now

	return nil
}

func (c *BwClient) findSecret(syncData *syncResponseType, secretName string) (map[string]string, error) {
	orgKeys, err := syncData.getOrgKeys(c.encryptionKey)
	if err != nil {
		return nil, err
	}
	// For secrets with no organisation, use the personal encryption key
	orgKeys[""] = c.encryptionKey

	for _, cipher := range syncData.Ciphers {
		// Determine which encryption key to use
		decryptionKey, ok := orgKeys[cipher.OrganizationID]
		if !ok {
			return nil, fmt.Errorf("Failed to find key for organisation '%s'", cipher.OrganizationID)
		}

		// Decrypt the name of the secret
		name, err := cipher.Name.decrypt(decryptionKey)
		if err != nil {
			return nil, err
		}
		if string(name) == secretName {
			// Found the secret - now decrypt the other details
			secret := make(map[string]string)
			if cipher.Login.Username.s != "" {
				username, err := cipher.Login.Username.decrypt(decryptionKey)
				if err != nil {
					return nil, err
				}
				secret["username"] = string(username)
			}
			if cipher.Login.Password.s != "" {
				password, err := cipher.Login.Password.decrypt(decryptionKey)
				if err != nil {
					return nil, err
				}
				secret["password"] = string(password)
			}
			if cipher.Notes.s != "" {
				notes, err := cipher.Notes.decrypt(decryptionKey)
				if err != nil {
					return nil, err
				}
				secret["notes"] = string(notes)
			}
			return secret, nil
		}
	}
	logger.Warn("Secret not found", "name", secretName)
	return nil, &NotFoundError{secretName}
}

// NotFoundError is returned when the requested secret was not found in the
// Bitwarden response
type NotFoundError struct {
	secretName string
}

func (e *NotFoundError) Error() string {
	return fmt.Sprintf("Not found: '%s'", e.secretName)
}

// bwCipher represents the interesting details of a Cipher in the
// Bitwarden sync response
type bwCipher struct {
	OrganizationID string       `json:"OrganizationId"`
	Type           int          `json:"Type"`
	Name           cipherString `json:"Name"`
	Notes          cipherString `json:"Notes"`
	Login          struct {
		Username cipherString `json:"Username"`
		Password cipherString `json:"Password"`
	} `json:"Login"`
}

type cipherString struct {
	s string
}

func (c cipherString) decrypt(key []byte) ([]byte, error) {
	return decryptCipherstring(c.s, key)
}

func (c *cipherString) UnmarshalText(text []byte) error {
	c.s = string(text)
	return nil
}
