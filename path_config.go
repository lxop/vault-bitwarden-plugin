package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

func (b *backend) pathConfig() *framework.Path {
	return &framework.Path{
		Pattern: "config",
		Fields: map[string]*framework.FieldSchema{
			"url": {
				Type:        framework.TypeString,
				Description: "URL of the Bitwarden server",
				Required:    true,
			},
		},
		Operations: map[logical.Operation]framework.OperationHandler{
			logical.UpdateOperation: &framework.PathOperation{
				Callback: b.operationConfigUpdate,
			},
			logical.ReadOperation: &framework.PathOperation{
				Callback: b.operationConfigRead,
			},
			logical.DeleteOperation: &framework.PathOperation{
				Callback: b.operationConfigDelete,
			},
		},
		HelpSynopsis:    pathConfigRootHelpSyn,
		HelpDescription: pathConfigRootHelpDesc,
	}
}

type bwConfig struct {
	URL string `json:"url"`
}

func (b *backend) operationConfigUpdate(ctx context.Context, req *logical.Request, data *framework.FieldData) (*logical.Response, error) {
	var url string
	if urlIfc, ok := data.GetOk("url"); ok {
		url = urlIfc.(string)
	} else {
		return nil, logical.CodedError(http.StatusBadRequest, "url is required")
	}
	logger.Info(fmt.Sprintf("Updating base URL to '%s'", url))

	entry, err := logical.StorageEntryJSON("config", bwConfig{
		URL: url,
	})
	if err != nil {
		return nil, fmt.Errorf("Failed to create StorageEntryJSON: %w", err)
	}
	if err := req.Storage.Put(ctx, entry); err != nil {
		return nil, fmt.Errorf("Failed to Put entry into storage: %w", err)
	}
	return nil, nil
}

func (b *backend) operationConfigRead(ctx context.Context, req *logical.Request, _ *framework.FieldData) (*logical.Response, error) {
	url, err := getBaseURL(ctx, req.Storage)
	if err != nil {
		switch err {
		case NotConfiguredError:
			// Not set yet
			return nil, nil
		default:
			return nil, err
		}
	}

	return &logical.Response{
		Data: map[string]interface{}{
			"url": url,
		},
	}, nil
}

// NotConfiguredError is raised when trying to access Bitwarden before
// the Bitwarden URL has been configured.
var NotConfiguredError = logical.CodedError(http.StatusUnprocessableEntity, "Bitwarden URL is not configured")

func getBaseURL(ctx context.Context, storage logical.Storage) (string, error) {
	entry, err := storage.Get(ctx, "config")
	if err != nil {
		return "", fmt.Errorf("Failed to get config from storage: %w", err)
	}
	if entry == nil {
		// Not set yet
		return "", NotConfiguredError
	}
	conf := bwConfig{}
	if err := entry.DecodeJSON(&conf); err != nil {
		return "", fmt.Errorf("Failed to decode stored config: %w", err)
	}

	return conf.URL, nil
}

func (b *backend) operationConfigDelete(ctx context.Context, req *logical.Request, _ *framework.FieldData) (*logical.Response, error) {
	if err := req.Storage.Delete(ctx, "config"); err != nil {
		return nil, fmt.Errorf("Failure while deleting config from storage: %w", err)
	}
	return nil, nil
}

const pathConfigRootHelpSyn = `
Configure the URL for the Bitwarden server
`

const pathConfigRootHelpDesc = `
To pull secrets from Bitwarden, Vault needs to know where to look. 
This endpoint is used to configure that location.
`
