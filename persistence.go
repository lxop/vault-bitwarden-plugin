package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/hashicorp/vault/sdk/logical"
)

type storedClientDetails struct {
	AccessToken   string    `json:"accessToken"`
	RefreshToken  string    `json:"refreshToken"`
	Expiry        time.Time `json:"expiry"`
	LastRefresh   time.Time `json:"lastRefresh"`
	EncryptionKey []byte    `json:"encryptionKey"`
}

func getClient(ctx context.Context, req *logical.Request) (BwClientIfc, error) {
	baseURL, err := getBaseURL(ctx, req.Storage)

	// Determine where to find the client details
	storageKey, err := getStorageKey(req)
	if err != nil {
		return nil, err
	}

	entry, err := req.Storage.Get(ctx, storageKey)
	if err != nil {
		return nil, fmt.Errorf("Error while getting auth details from storage: %w", err)
	}
	client := &BwClient{
		baseURL: baseURL,
	}
	if entry != nil {
		details := storedClientDetails{}
		if err := entry.DecodeJSON(&details); err != nil {
			return nil, fmt.Errorf("Error decoding auth details")
		}
		client.accessToken = details.AccessToken
		client.refreshToken = details.RefreshToken
		client.expiry = details.Expiry
		client.lastRefresh = details.LastRefresh
		client.encryptionKey = details.EncryptionKey
	}
	return client, nil
}

func storeClient(ctx context.Context, req *logical.Request, client BwClientIfc) error {
	storageKey, err := getStorageKey(req)
	if err != nil {
		return err
	}
	details := &storedClientDetails{
		AccessToken:   client.getAccessToken(),
		RefreshToken:  client.getRefreshToken(),
		Expiry:        client.getExpiry(),
		LastRefresh:   client.getLastRefresh(),
		EncryptionKey: client.getEncryptionKey(),
	}
	entry, err := logical.StorageEntryJSON(storageKey, details)
	if err != nil {
		return fmt.Errorf("Error creating storage entry for auth details: %w", err)
	}
	if err := req.Storage.Put(ctx, entry); err != nil {
		return fmt.Errorf("Error putting auth details into storage: %w", err)
	}
	return nil
}

func getStorageKey(req *logical.Request) (string, error) {
	if req.EntityID == "" {
		return "", logical.CodedError(http.StatusForbidden, "endpoint only available to callers tied to an identity")
	}
	key := fmt.Sprintf("auth/%s", req.EntityID)
	return key, nil
}
